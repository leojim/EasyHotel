﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMSOFT.EasyHotel.DAL;

namespace IEMSOFT.EasyHotel.Admin.Lib
{
    public class RoleBLL
    {
        public static List<SelectListItem> GetRoles()
        {
            List<SelectListItem> options;
            using (var db = new EasyHotelDB())
            {
                var dal = new RoleDAL(db);
                var ret = dal.GetList();
                options = ret.Select((item) => new SelectListItem
                {
                    Text = item.Name,
                    Value = item.RoleId.ToString()
                }).ToList();
            }
            return options;
        }
    }
}