﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMSOFT.EasyHotel.DAL;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.EasyHotel.Common;
using IEMSOFT.EasyHotel.Admin.Lib;
using IEMSOFT.Foundation;
using IEMSOFT.EasyHotel.Admin.Models;
namespace IEMSOFT.EasyHotel.Admin.Controllers
{
    public class ReportController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.CurrentUser = CurrentUser;
            return View();
        }

        public ActionResult SearchDayReport(DayReportSearchConditionModel model)
        {
            var ret = new BasicResult<List<BillFullModel>, List<string>>();
            ret.Msg = new List<string>();
            if (!string.IsNullOrEmpty(model.EndDate))
            {
                model.EndDate = model.EndDate.ToDateTime().AddDays(1).ToShortDateStr();
            }
            int? userId = null;
            if(CurrentUser.RoleId==RoleType.Reception.ToInt())
            {
                userId=CurrentUser.UserId;//如果是前台，只需要查出前台操作的订单
            }
            if(CurrentUser.RoleId!=RoleType.GroupLeader.ToInt())
            {
                model.SubHotelId=CurrentUser.SubHotelId;
            }
            ret.Data = BillBLL.SearchFinishedBills(model.FromDate,
                                                   model.EndDate,
                                                   model.RoomTypeId,
                                                   model.TravelAgencyId,
                                                   model.PayTypeId,
                                                   model.SubHotelId,
                                                   userId,
                                                   CurrentUser.GroupHotelId);
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult PrintMonth(MonthReportSearchConditionModel model)
        {
            var ret = new PrintMonthModel();
            var fromDate = string.Format("{0}-{1}-01", model.FromYear, model.FromMonth);
            var endDate = string.Format("{0}-{1}-01", model.EndYear, model.EndMonth);
            ret.StartMonth = fromDate.ToDateTime().ToString("yyyyMM");
            ret.EndMonth = endDate.ToDateTime().ToString("yyyyMM");
            endDate = endDate.ToDateTime().AddMonths(1).ToShortDateStr();
            int? userId = null;
            if (CurrentUser.RoleId == RoleType.Reception.ToInt())
            {
                userId = CurrentUser.UserId;//如果是前台，只需要查出前台操作的订单
            }
            if (CurrentUser.RoleId != RoleType.GroupLeader.ToInt())
            {
                model.SubHotelId = CurrentUser.SubHotelId;
            }
            var monthReport = BillBLL.SearchMonthReport(fromDate,
                                                    endDate,
                                                    model.RoomTypeId,
                                                    model.TravelAgencyId,
                                                    model.PayTypeId,
                                                    model.SubHotelId,
                                                    userId,
                                                    CurrentUser.GroupHotelId);
            ret.Reports = monthReport;
            ret.Reports = ret.Reports ?? new List<MonthReportMode>();
            ret.BillsCountSum = ret.Reports.Sum(item => item.BillsCount);
            ret.RoomFeeSum = ret.Reports.Sum(item => item.BillsRoomTotalFee);
            ret.ConsumeFeeSum = ret.Reports.Sum(item => item.BillsConsumeTotalFee);
            ret.TotalFee = ret.RoomFeeSum + ret.ConsumeFeeSum;
            if (model.SubHotelId == null)
                ret.SubHotelName = "集团全部酒店";
            else
            {
                using (var db=new EasyHotelDB())
                {
                    var sub = new SubHotelDAL(db);
                    var subHotel=sub.GetOne(model.SubHotelId.Value);
                    ret.SubHotelName=subHotel==null?"":subHotel.Name;
                }
            }
            return PartialView("PrintMonthReport", ret);
        }

        [HttpPost]
        public ActionResult PrintDay(DayReportSearchConditionModel model)
        {
            var endDate = model.EndDate;
            var ret = new PrintDayReportModel();
            if (!string.IsNullOrEmpty(model.EndDate))
            {
                model.EndDate = model.EndDate.ToDateTime().AddDays(1).ToShortDateStr();
            }
            int? userId = null;
            if (CurrentUser.RoleId == RoleType.Reception.ToInt())
            {
                userId = CurrentUser.UserId;//如果是前台，只需要查出前台操作的订单
            }
            if (CurrentUser.RoleId != RoleType.GroupLeader.ToInt())
            {
                model.SubHotelId = CurrentUser.SubHotelId;
            }
            ret.FromDate = model.FromDate;
            ret.EndDate = endDate;
            ret.Bills = BillBLL.SearchFinishedBills(model.FromDate,
                                                   model.EndDate,
                                                   model.RoomTypeId,
                                                   model.TravelAgencyId,
                                                   model.PayTypeId,
                                                   model.SubHotelId,
                                                   userId,
                                                   CurrentUser.GroupHotelId);
            ret.Bills = ret.Bills ?? new List<BillFullModel>();
            ret.RoomFeeSum = ret.Bills.Sum(item => item.RoomTotalFee);
            ret.ConsumeFeeSum = ret.Bills.Sum(item => item.ConsumeFee);
            ret.TotalFee = ret.RoomFeeSum + ret.ConsumeFeeSum;
            return PartialView("PrintDayReport", ret);
        }

        public ActionResult SearchMonthReport(MonthReportSearchConditionModel model)
        {
            var ret = new BasicResult<List<MonthReportMode>, List<string>>();
            ret.Msg = new List<string>();
            var fromDate = string.Format("{0}-{1}-01", model.FromYear, model.FromMonth);
            var endDate = string.Format("{0}-{1}-01", model.EndYear, model.EndMonth);
            endDate = endDate.ToDateTime().AddMonths(1).ToShortDateStr();
             int? userId = null;
            if(CurrentUser.RoleId==RoleType.Reception.ToInt())
            {
                userId=CurrentUser.UserId;//如果是前台，只需要查出前台操作的订单
            }
            if(CurrentUser.RoleId!=RoleType.GroupLeader.ToInt())
            {
                model.SubHotelId=CurrentUser.SubHotelId;
            }
           var monthReport = BillBLL.SearchMonthReport(fromDate,
                                                   endDate,
                                                   model.RoomTypeId,
                                                   model.TravelAgencyId,
                                                   model.PayTypeId,
                                                   model.SubHotelId,
                                                   userId,
                                                   CurrentUser.GroupHotelId);
           ret.Data = monthReport;
            return JsonNet(ret);
        }
    }
}
