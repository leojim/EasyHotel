﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMSOFT.EasyHotel.DAL.Models
{
    [PetaPoco.TableName("PayType")]
    [PetaPoco.PrimaryKey("PayTypeId", autoIncrement = true)]
    public class PayType
    {
        public int PayTypeId { get; set; }
        public string Name { get; set; }
        private DateTime _created = DateTime.Now;
        public DateTime Created { get { return _created; } set { _created = value; } }
        private DateTime _modified = DateTime.Now;
        public DateTime Modified { get { return _modified; } set { _modified = value; } }
    }
}
