﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMSOFT.EasyHotel.DAL.Models
{
    [PetaPoco.TableName("SubHotel")]
    [PetaPoco.PrimaryKey("SubHotelId", autoIncrement = true)]
    public class SubHotel
    {
        public int SubHotelId { get; set; }
        public int GroupHotelId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Fax { get; set; }
        public string Phone { get; set; }
        private DateTime _created = DateTime.Now;
        public DateTime Created { get { return _created; } set { _created = value; } }
        private DateTime _modified = DateTime.Now;
        public DateTime Modified { get { return _modified; } set { _modified = value; } }
    }
}
